package typescriptify

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
)

type TypeScriptify struct {
	prefix      string
	golangTypes []reflect.Type
	types       map[reflect.Kind]string

	// throwaway, used when converting
	alreadyConverted map[reflect.Type]bool
}

func New() *TypeScriptify {
	result := new(TypeScriptify)

	types := make(map[reflect.Kind]string)

	types[reflect.Bool] = "boolean"

	types[reflect.Int] = "number"
	types[reflect.Int8] = "number"
	types[reflect.Int16] = "number"
	types[reflect.Int32] = "number"
	types[reflect.Int64] = "number"
	types[reflect.Uint] = "number"
	types[reflect.Uint8] = "number"
	types[reflect.Uint16] = "number"
	types[reflect.Uint32] = "number"
	types[reflect.Uint64] = "number"
	types[reflect.Float32] = "number"
	types[reflect.Float64] = "number"

	types[reflect.String] = "string"

	result.types = types

	return result
}

func (this TypeScriptify) getTypescriptFieldLine(fieldName string, kind reflect.Kind, array bool) (string, error) {
	arr := ""
	if array {
		arr = "[]"
	}
	if typeScriptType, ok := this.types[kind]; ok {
		if len(fieldName) > 0 {
			return fmt.Sprintf("\t%s : %s%s;\n", fieldName, typeScriptType, arr), nil
		}
	}
	return "", errors.New(fmt.Sprintf("don't know how to translate type %s (field:%s)", kind.String(), fieldName))
}

func (this *TypeScriptify) Prefix(prefix string) {
	this.prefix = prefix
}

func (this *TypeScriptify) Add(obj interface{}) {
	this.AddType(reflect.TypeOf(obj))
}

func (this *TypeScriptify) AddType(typeOf reflect.Type) {
	this.golangTypes = append(this.golangTypes, typeOf)
}

func (this *TypeScriptify) Convert() (string, error) {
	this.alreadyConverted = make(map[reflect.Type]bool)

	result := ""
	for _, typeof := range this.golangTypes {
		typeScriptCode, err := this.convertType(typeof)
		if err != nil {
			return "", err
		}
		result += "\n" + strings.Trim(typeScriptCode, " \t\r\n")
	}
	return result, nil
}

func (this *TypeScriptify) convertType(typeOf reflect.Type) (string, error) {
	result := fmt.Sprintf("interface %s%s {\n", this.prefix, typeOf.Name())

	if _, found := this.alreadyConverted[typeOf]; found {
		// Already converted
		return "", nil
	}

	for i := 0; i < typeOf.NumField(); i++ {
		val := typeOf.Field(i)
		//fmt.Println("kind=", val.Type.Kind().String())
		jsonTag := val.Tag.Get("json")
		jsonFieldName := ""
		if len(jsonTag) > 0 {
			jsonTagParts := strings.Split(jsonTag, ",")
			if len(jsonTagParts) > 0 {
				jsonFieldName = strings.Trim(jsonTagParts[0], " \t")
			}
		}
		if len(jsonFieldName) > 0 && jsonFieldName != "-" {
			if val.Type.Kind() == reflect.Struct {
				// Struct:
				typeScriptChunk, err := this.convertType(val.Type)
				if err != nil {
					return "", err
				}
				result = typeScriptChunk + "\n" + result + fmt.Sprintf("\t%s : %s;\n", jsonFieldName, val.Type.Name())
			} else if val.Type.Kind() == reflect.Slice {
				// Slice:
				if val.Type.Elem().Kind() == reflect.Struct {
					// Slice of structs:
					typeScriptChunk, err := this.convertType(val.Type.Elem())
					if err != nil {
						return "", err
					}
					result = typeScriptChunk + "\n" + result + fmt.Sprintf("\t%s : %s[];\n", jsonFieldName, val.Type.Elem().Name())
				} else {
					// Slice of simple fields:
					line, err := this.getTypescriptFieldLine(jsonFieldName, val.Type.Elem().Kind(), true)
					if err != nil {
						return "", err
					}
					result += line
				}
			} else {
				// simple field:
				line, err := this.getTypescriptFieldLine(jsonFieldName, val.Type.Kind(), false)
				if err != nil {
					return "", err
				}
				result += line
			}
		}
	}
	result += "}"

	this.alreadyConverted[typeOf] = true

	return result, nil
}

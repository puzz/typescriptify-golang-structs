# A Golang JSON to TypeScript interface converter

    converter := typescriptify.New()
    converter.Add(Person{})
    converter.Add(Dummy{})
    string, err := converter.Convert()
    if err != nil {
        panic(err.Error())
    }

If the `Person` structs contain a reference to the `Address` struct, then you don't have to add `Address explicitly`. Only fields with a valid `json` tag will be converted to TypeScript interfaces.

Example input structs:

    type Address struct {
        // Used in html
        Duration float64 `json:"duration"`
        Text1    string  `json:"text,omitempty"`
        // Ignored:
        Text2 string `json:",omitempty"`
        Text3 string `json:"-"`
    }

    type Dummy struct {
        Something string `json:"something"`
    }

    type Person struct {
        Name      string    `json:"name"`
        Nicknames []string  `json:"nicknames"`
        Addresses []Address `json:"addresses"`
        Dummy     Dummy     `json:"a"`
    }

Generated TypeScript:

    interface Dummy {
            something : string;
    }
    interface Address {
            duration : number;
            text : string;
    }
    interface Person {
            name : string;
            nicknames : string[];
            addresses : Address[];
            a : Dummy;
    }

Then in TypeScript you can just cast your JSON in any of those interfaces:

    var json = <Person> {"name":"Me myself","nicknames":["aaa", "bbb"]};
    console.log(json.name);
    // The TypeScript compiler will throw an error for this line
    console.log(json.something);

If you use golang JSON structs as responses from your API, you may want to have a common prefix for all the generated interfaces:

    converter := typescriptify.New()
    converter.Prefix("API_")
    converter.Add(Person{})

The interface name will be `API_Person` instead of `Person`.

License
-------

This library is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

